import Combine

/*let just = Just(4)

just.sink { value in
    print("value: \(value)")
}

let passSubject = PassthroughSubject<Int, Never>()

let subject = passSubject.sink { value in
    print("value: \(value)")
}

passSubject.send(5)
 */

@propertyWrapper
struct UpperCased {
    var wrappedValue: String {
        didSet {
            wrappedValue = wrappedValue.uppercased()
        }
    }
    
    init(wrappedValue: String) {
        self.wrappedValue = wrappedValue.uppercased()
    }
}

@propertyWrapper
struct CustomPublisher<T> {
    private var value: T
    private let subject: CurrentValueSubject<T, Never>
    
    var wrappedValue: T {
        get {
            return value
        }
        set {
            self.value = newValue
            subject.send(newValue)
        }
    }
    
    var projectedValue: CurrentValueSubject<T, Never> {
        subject
    }
    
    init(wrappedValue: T) {
        self.value = wrappedValue
        self.subject = CurrentValueSubject(value)
    }
}

class Person {
    @UpperCased var name: String = "lowercased"
    @CustomPublisher var importantValue: String = "something"
    
}

let person = Person()
print(person.name)
person.name = "touppercase"
person.$importantValue.sink { value in
    print("value: \(value)")
}
print(person.name)
//print(person.$name)
