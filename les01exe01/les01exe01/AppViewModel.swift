//
//  AppViewModel.swift
//  les01exe01
//
//  Created by Александр Титков on 30.11.2023.
//

import Foundation

class AppViewModel: ObservableObject {
    @Published var displayedTab: TabOptions = .tab1
    @Published var isNavlink1Active: Bool = false
    @Published var isNavlink2Active: Bool = false
    @Published var isSheetPresented: Bool = false
}
