//
//  ContentView.swift
//  les01exe01
//
//  Created by Александр Титков on 30.11.2023.
//

import SwiftUI

struct ContentView: View {
    @StateObject var appViewModel = AppViewModel()
    
    var body: some View {
        TabView(selection: self.$appViewModel.displayedTab) {
            Tab1View(appViewModel: appViewModel)
                .tag(TabOptions.tab1)
                .tabItem {
                    Image(systemName: "1.circle")
                }
            
            Tab2View(appViewModel: appViewModel)
                .tag(TabOptions.tab2)
                .tabItem {
                    Image(systemName: "2.circle")
                }
            
            Tab3View(appViewModel: appViewModel)
                .tag(TabOptions.tab3)
                .tabItem {
                    Image(systemName: "3.circle")
                }
                .sheet(isPresented: $appViewModel.isSheetPresented) {
                    Text("Sheet is opened")
                }
            
            Tab4View(appViewModel: appViewModel)
                .tag(TabOptions.tab4)
                .tabItem {
                    Image(systemName: "4.circle")
                }
        }
    }
}

#Preview {
    ContentView()
}
