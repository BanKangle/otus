//
//  UIView.swift
//  les01exe01
//
//  Created by Александр Титков on 02.12.2023.
//

import UIKit
import SwiftUI

struct CustomUIView: UIViewRepresentable {
    //typealias UIViewType = UIActivityIndicatorView
    var style: UIActivityIndicatorView.Style
    var isLoading: Bool
    
    func makeUIView(context: Context) -> UIActivityIndicatorView {
        return UIActivityIndicatorView(style: style)
    }
    
    func updateUIView(_ uiView: UIActivityIndicatorView, context: Context) {
        if isLoading {
            uiView.startAnimating()
        } else {
            uiView.stopAnimating()
        }
    }
}
