//
//  Tab1View.swift
//  les01exe01
//
//  Created by Александр Титков on 30.11.2023.
//

import SwiftUI

struct Tab1View: View {
    @StateObject var appViewModel: AppViewModel
    
    var body: some View {
        VStack {
            Spacer()
            Text("OTUS")
                .font(.title)
            Text("iOS Professional")
                .font(.title)
            Text("lesson 01 exercise 01")
                .font(.title)
            Spacer()
            
            VStack {
                Button {
                    self.appViewModel.displayedTab = .tab2
                    self.appViewModel.isNavlink1Active.toggle()
                } label: {
                    Text("Navigate to tab 2 navlink 1")
                }
                Divider()
                Button {
                    self.appViewModel.displayedTab = .tab2
                    self.appViewModel.isNavlink2Active.toggle()
                } label: {
                    Text("Navigate to tab 2 navlink 2")
                }
                Divider()
                Button {
                    self.appViewModel.displayedTab = .tab3
                    self.appViewModel.isSheetPresented.toggle()
                } label: {
                    Text("Navigate to tab 2's sheet")
                }
            }
            Spacer()
        }
    }
}

#Preview {
    Tab1View(appViewModel: AppViewModel())
}
