//
//  Tab2View.swift
//  les01exe01
//
//  Created by Александр Титков on 30.11.2023.
//

import SwiftUI

struct Tab2View: View {
    @StateObject  var appViewModel: AppViewModel
    
    var body: some View {
        VStack {
            NavigationView {
                List {
                    NavigationLink(isActive: $appViewModel.isNavlink1Active) {
                        Text("Navigation link 1")
                    } label: {
                        Text("Navigation link 1")
                    }
                    NavigationLink(isActive: $appViewModel.isNavlink2Active) {
                        Text("Navigation link 2")
                    } label: {
                        Text("Navigation link 2")
                    }
                }
                .navigationTitle("Navlinks")
            }
        }
    }
}

#Preview {
    Tab2View(appViewModel: AppViewModel())
}
