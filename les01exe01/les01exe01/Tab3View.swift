//
//  Tab3View.swift
//  les01exe01
//
//  Created by Александр Титков on 30.11.2023.
//

import SwiftUI

struct Tab3View: View {
    @StateObject var appViewModel: AppViewModel
    
    var body: some View {
        VStack {
            Button {
                self.appViewModel.isSheetPresented.toggle()
            } label: {
                Text("Show modal")
            }
        }
    }
}

#Preview {
    Tab3View(appViewModel: AppViewModel())
}
