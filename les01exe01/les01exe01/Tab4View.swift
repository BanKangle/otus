//
//  Tab4View.swift
//  les01exe01
//
//  Created by Александр Титков on 02.12.2023.
//

import SwiftUI

struct Tab4View: View {
    @StateObject var appViewModel: AppViewModel
    @State var isLoading: Bool = false
    
    var body: some View {
        VStack {
            Spacer()
            CustomUIView(style: .large, isLoading: isLoading)
            Spacer()
            Button {
                isLoading.toggle()
            } label: {
                Text("Toggle")
            }
            Spacer()
        }
    }
}

#Preview {
    Tab4View(appViewModel: AppViewModel())
}
