//
//  TabOptions.swift
//  les01exe01
//
//  Created by Александр Титков on 30.11.2023.
//

import Foundation

enum TabOptions {
    case tab1
    case tab2
    case tab3
    case tab4
}
