//
//  les01exe01App.swift
//  les01exe01
//
//  Created by Александр Титков on 30.11.2023.
//

import SwiftUI

@main
struct les01exe01App: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
